#include <ros/ros.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

// File managing
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>


geometry_msgs::Point rob0_pos_;
geometry_msgs::Point rob1_pos_;
geometry_msgs::Point goal_pos;
std::ofstream log_data_file;
std::string data_log_filename;


void robot0_pose_callback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg_rob0_pose)
{
    rob0_pos_ = msg_rob0_pose->pose.pose.position;
   // ROS_INFO("[STOP SIM :: robot0_pose_callback] - Pos x %f - y %f", rob0_pos_.x, rob0_pos_.y);

}
void robot1_pose_callback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg_rob1_pose)
{
    rob1_pos_ = msg_rob1_pose->pose.pose.position;
   // ROS_INFO("[STOP SIM :: robot1_pose_callback] - Pos x %f - y %f", rob1_pos_.x, rob1_pos_.y);

}

void stop_simulation()
{

    ROS_INFO("[STOP SIM :: stop_simulation] Killing nodes");
    std::system("rosnode kill /move_obstacles");
    std::system("rosnode kill /planned_prediction");
    sleep(1);
    std::system("rosnode kill /stageros");
}

double point_distance (const geometry_msgs::Point point_1, const geometry_msgs::Point point_2)
{
    double distance;
    double distance_x;
    double distance_y;

    distance_x = point_1.x - point_2.x;
    distance_y = point_1.y - point_2.y;

    distance = std::sqrt(distance_x*distance_x + distance_y*distance_y);

    return distance;
}

std::string get_date(bool get_hour)
{
  std::time_t now;
  char the_date[12];

  the_date[0] = '\0';

  now = std::time(NULL);

  if (now != -1)
  {
    if(get_hour)
    {
      std::strftime(the_date, 12, "%H:%M:%S", std::localtime(&now));
    }
    else
    {
      std::strftime(the_date, 12, "%d_%m-%H_%M", std::localtime(&now));
    }
  }

  return std::string(the_date);
}

bool open_log_file()
{
    bool is_open;
    try
    {
        log_data_file.open(data_log_filename.c_str(), std::ios::app);
        is_open = true;
    }
    catch (std::ofstream::failure &writeErr)
    {
        ROS_ERROR("Exception occured when writing to a file - %s", writeErr.what());
        is_open = false;
    }
    return is_open;
}

bool write_log_header()
{
  if (open_log_file())
  {
    //ROS_INFO("[STOP SIM::write_log_header] Create Log data file Header");
    log_data_file << "Time Stamp, ";
    log_data_file << "Target_pos_x, ";
    log_data_file << "Target_pos_y, ";
    log_data_file << "Robot_pose_x, ";
    log_data_file << "Robot_pose_y, ";
    log_data_file << "D_to_pursuer, ";
    log_data_file << "D_to_goal, ";
    log_data_file << "Intercepted, ";
    log_data_file << "Not Intercepted";
    log_data_file << "\n";
    log_data_file.close();
    return true;
  }
  return false;
}

void store_info(const int intercepted)
{
    if (open_log_file())
    {
      std::string tmp_string;
      char buffer[100];
      // Time Stamp
      tmp_string = get_date(true) + std::string(", ");
      log_data_file << tmp_string.c_str();

      // Target_pos_x
      // Target_pos_y

      tmp_string.clear();
      sprintf(buffer, "%4f, %.4f, ", rob1_pos_.x, rob1_pos_.y);
      tmp_string = buffer;
      log_data_file << tmp_string.c_str();

      // Robot_pose_x
      // Robot_pose_y

      tmp_string.clear();
      sprintf(buffer, "%4f, %.4f, ", rob0_pos_.x, rob0_pos_.y);
      tmp_string = buffer;
      log_data_file << tmp_string.c_str();

      // D_to_pursuer
      tmp_string.clear();
      sprintf(buffer, "%.3f, ", point_distance (rob0_pos_, rob1_pos_));
      tmp_string = buffer;
      log_data_file << tmp_string.c_str();



      // D_to_goal
      tmp_string.clear();
      sprintf(buffer, "%.3f, ", point_distance (goal_pos, rob1_pos_));
      tmp_string = buffer;
      log_data_file << tmp_string.c_str();

      if (intercepted == -1)
      {
          //Not -  Intercepted    // Not -  NotIntercepted
          tmp_string.clear();
          sprintf(buffer, "0, 0");
          tmp_string = buffer;
          log_data_file << tmp_string.c_str();



      }
      else if (intercepted == 0)
      {
          // Not Intercepted
          tmp_string.clear();
          sprintf(buffer, "0, 1");
          tmp_string = buffer;
          log_data_file << tmp_string.c_str();
      }
      else if (intercepted == 1)
      {
          // Intercepted
          tmp_string.clear();
          sprintf(buffer, "1, 0");
          tmp_string = buffer;
          log_data_file << tmp_string.c_str();

      }
      log_data_file << "\n";
      log_data_file.close();

    }
    else
    {
      ROS_WARN("[EurathlonControl::store_info] log data file not open");
    }
    log_data_file.flush();

}

void timerCallback(const ros::TimerEvent& event)
{
    ROS_DEBUG("[[STOP SIM] -store_info - log filename %s", data_log_filename.c_str());
    store_info(-1);

}


int main(int argc, char **argv){

  ros::init(argc, argv, "stop_sim");

  ROS_INFO("[STOP SIM] - Start node");

  ros::NodeHandle nh;
  int ret_val;


  std::string data_log_folder;
  double prediction_length;
  double store_log_freq;
  int intercepted;

  intercepted = -1;


  nh.param("goal_pos_x", goal_pos.x, 3.1);
  nh.param("goal_pos_y", goal_pos.y, -18.0);
  nh.param("data_log_folder", data_log_folder, std::string("/home/mario/risk_rrt_logs/"));
  nh.param("store_data_freq", store_log_freq, 1.0);
  nh.param("/planned_prediction/prediction_time", prediction_length, 19.0);
//  nh.param("/robot_1/filter_xy_node/path_time", prediction_length, 19.0);

  ros::Subscriber sub_pos1 = nh.subscribe("/robot_0/amcl_pose",1, robot0_pose_callback);
  ros::Subscriber sub_pos2 = nh.subscribe("/robot_1/amcl_pose",1, robot1_pose_callback);

  ros::Timer store_info_timer;

  ros::Rate loop_rate(1.0);

  std::string tmp_string;
  char buffer[100];
  tmp_string.clear();
  sprintf(buffer, "%d",(int)(prediction_length));
  tmp_string = buffer;
  data_log_filename = data_log_folder + "log_RRT_";
  data_log_filename = data_log_filename + get_date(false);
  data_log_filename = data_log_filename + "_PlanPred_";
  data_log_filename = data_log_filename + tmp_string.c_str();
  data_log_filename = data_log_filename + ".txt";

  log_data_file.exceptions (std::ifstream::failbit | std::ifstream::badbit);

  if( !write_log_header() )
  {
     ROS_ERROR("[STOP SIM] Error in log file");
  }
  else
  {
    ROS_INFO("[STOP SIM] Logging to %s", data_log_filename.c_str());
    store_info_timer = nh.createTimer(ros::Duration(1.0/store_log_freq), timerCallback);
    ROS_INFO("[STOP SIM] Node Initialization Completed");

  }

  ROS_INFO("[STOP SIM] - Spinnig");
  while (ros::ok())
  {
      double distance;
      distance = point_distance (rob0_pos_, rob1_pos_);

      if (distance <= 2.0 && distance > 0 )
      {
         intercepted = 1;
          ROS_INFO("[STOP SIM] - Intercepted");
      }
      else
      {
          distance = point_distance (goal_pos, rob1_pos_);
          if (distance <= 1.4 && distance > 0 )
          {
              ROS_INFO("[STOP SIM] - Not Intercepted");
              intercepted = 0;
          }
      }
      if(intercepted >= 0)
      {
          store_info_timer.stop();
          stop_simulation();
          store_info(intercepted);
          ros::shutdown();
      }

      ros::spinOnce();
      loop_rate.sleep();


  }


  return  0;


}
